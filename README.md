Bearing Blocks
==============

I purchased KP08 bearing pillow blocks and was disappointed by their
low quality; the axle height was inconsistent from block to block in
a set, and many had damaged balls in the bearing races.  I did not
have any use for the self-centering feature in my design anyway,

This provides a drop-in replacement for some uses of KP08
bearing blocks using 608 (8mm ID x 22mm OD x 7mm thick) bearings.
The limitations include:

* The 608 bearings do not have a locking collar attached to the
  bearing. If you need to lock the axle against a side load, you
  can either use a separate locking collar on each side or use
  thread lock compound to hold the axle in place in the bearing.
* There is no self-centering feature; the axle must be straight
  and parallel to the mounting feature.  That said, there is
  enough compliance in the plastic to accomodate a slight
  variance.
* The single bearing block is approximately 2mm wider than a KP08.
  This is intended to give a wider stance to keep the block straight
  to avoid binding from having no self-centering feature.

This was designed in the LinkStage3 branch of FreeCAD.  This design
can be adapted to other bearing styles. All of the critical
dimensions that need to adapt are driven from a spreadsheet.

I print in PETG for toughness and dimensional stability. If you
use another filament, you may have to adjust dimensions or even
the design. PLA is sufficiently brittle that it might crack when
the bearing is inserted. The parts are presented in their proper
printing orientation, and require no supports. Post processing
might require light filing under the feet to remove protrusions
that might otherwise make the bearing axis not parallel to the
mounting surface.

The bearings are designed to be fixed with M5 machine screws.
For extrusion compatible with OpenBuilds V-slot or similar T-slot,
an M5x8 screw will be appropriate.

The **insertion tool** registers to the inner bore, but bears only
on the outer race in order to avoid damage to the balls or raceway.
It is used to press-fit the 608 bearings into most of these parts.

The **single bearing block** takes one 608 bearing. Use the insertion
tool to seat a bearing all the way in to the flange.  You should
be able to seat it by hand; if you have to use a hammer, increase
clearance in the model, or scale the print slightly, and print again.

The **double bearing block** takes one 608 bearing in each end, which
is pressed flat up to the flange and should be flush with the face.
There are three holes in each foot; the outer two on each foot
are on a 20mm pitch for installation on typical 4040 v-slot or
equivalent t-slot, and the center hole is for installation when
only a center slot is available.  The double bearing block might
be useful for cantilevered rods; at least, that is my use case.
In my use, I put a small drop of thread lock on the inside of
the inner race of one bearing, making sure none of it touches
the seal.

Some of the remaining parts were specifically designed for the
[monocle](https://project-monocle.gitlab.io/) but may be useful
otherwise. They are less thoroughly parameterized.

The **bed stage nut bearing block** mates a 8mm lead screw nut
to 2060 extrusion, as used on the bed frame in the Monocle.

The **thrust bearing block** and **radial bearing block** are
asymmetric to align with the bed stage nut bearking block.
The thrust bearing block holds an 8mm lead screw on top of
an [F3-8M thrust bearing](https://www.amazon.com/gp/product/B081JCSD6Z)
with a 608 bearing to take radial loads and the thrust bearing
to take axial loads. It is paired with the radial bearing block
at the top to avoid lead screw whipping. In both cases, use
the insertion tool to install the 608 bearing. The F3-8M
bearing comes in three pieces and is **not** captive. It is held
in place only by grease. Be sure to grease it with a high quality
grease appropriate for your use before using. Lithium grease is
an excellent choice for most applications; this is not meant to
be used for high-speed appliations.

The **idler post** and **idler race** together turn a 608 bearing
into an idler pully for 6mm timing belt. First use the insertion tool
to press the 608 bearing into the flanged race. The post **must**
be cleaned to pass an M5 screw before inserting it into the 608
bearing, because once it is inserted it is nearly impossible to
clean. (The idler post is the only piece that uses crush ribs to
fit inside the 608 bearing.)  Affix the idler pulley using an M5
screw through a wide M5 washer engaging the inner race of the 608
bearing. The flange on the bottom of the post should hold the
flanged race proud of the surface and it should turn freely when
installed.
